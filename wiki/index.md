# holowiki, for Peer-to-peer Collective Intelligence

[[holowiki]] is the first product to demonstrate the capability of [[holotools]]

each [[markdown]] files are simply "automagically" interlinked with [tags][holotag].

A tag is a [#markdown] link which doesn't have a referenced URL and is writen
 like ''[link text][linktag]'', the page name is the first word of the title.

~@michelc

tags: [[wiki]], [[holotools]], 

action: [edit] [publish] [share]
