# redirectors: ISP, connections, & Misc. ...

* mfs <http://127.0.0.1:5001/webui/#/files/>
* ipfs <http://127.0.0.1:8080/>
* local <http://0.0.0.0/> 
* <http://1.1.1.1/> (stolen by [cloudflare][1] !)
* <http://www.msftconnecttest.com/redirect>


[1]: https://duckduckgo.com/?q=when+cloudflare+stole+the+internet+1.1.1.1
