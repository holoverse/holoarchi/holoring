## holoX

from 2/25/2020 call

### holoModules (user icons)

 - holoBoard (universal dashboard, personalized)
 - holoCode (Code of Conduct)
 - holoContract (resonance contract, onboarding)

 - holoGuides (tutorial, publishing, curation)
 - holoHub (API marketplace, holoPlay, holoStore etc..)
 - holoConnect (communication, IM, RTC, chat, video conf etc. short-term memo)
 - holoMemory (long-term memory, akashic record)
 - holoMaps (discovery, navigation maps)
 - holoLicence (unlicence 4.0, 5.0)
 - holoGov (circle decision tools, votes, social optimizer, autonomous governance, self-sovereign responsibilisation tools and help etc...)

 - holoKit (package manager)

 - holoHome (containers)


### holoComponents (

 - holoValueSystem
 - holoHealth
 - holoStamp
 - holoTools (toolbox, utilities, productivity etc.)


### holoPlugins
 - holoView (visualization)
 - holoGraph (graphing...)

### holoPackages

 - holoRoot (SDK,environment)

### holoPartikis

 - holoHeart (core system)
 - UI
 - Frequency Coherence Meters
 - holoRing ledger
 - holoBot Smart Contract
 - holoID  holoPass (identity / passport)
 - holoCryption (security / encryption
 - holoKeys (PKI)
 - holoValue (token, coSense, etc. CSAP) 
 - holoBridge (interoperability)
 - holoCode (living code)

 
